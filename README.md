This is a small standalone particle system I've written for a custom engine called LightCore. For now it's 2D only, but I plan to expand it to 3D 
and integrate it with the rest of the engine's subsystems.

DEMO: https://www.youtube.com/watch?v=M-l6K2-Ax8k
